﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net;
using System.Net.NetworkInformation;
using System.Management;
using System.IO;

namespace Mercury.HabboEncryption
{
    class Security
    {
        private string[] names;
        private string[] machines;
        private static string GetMacAddress()
        {
            string macAddresses = "";
            foreach (NetworkInterface nic in NetworkInterface.GetAllNetworkInterfaces())
            {
                // Only consider Ethernet network interfaces, thereby ignoring any
                // loopback devices etc.
                if (nic.NetworkInterfaceType != NetworkInterfaceType.Ethernet)
                {

                }
                else if (nic.NetworkInterfaceType != NetworkInterfaceType.Wireless80211)
                {

                }
                else
                    continue;
                if (nic.OperationalStatus == OperationalStatus.Up)
                {
                    macAddresses += nic.GetPhysicalAddress().ToString();
                    break;
                }
            }
            return macAddresses;
        }

        internal void AllowedAddress()
        {
            names = new string[] 
            { 
                "782BCBBF8DCA" 
            };
        }

        internal void AllowedMachines()
        {
            machines = new string[]
            {
                "6A9FEBF000EE6DDFBFF"
            };
        }

        private static string getUniqueID(string drive)
        {
            if (drive == string.Empty)
            {
                //Find first drive
                foreach (DriveInfo compDrive in DriveInfo.GetDrives())
                {
                    if (compDrive.IsReady)
                    {
                        drive = compDrive.RootDirectory.ToString();
                        break;
                    }
                }
            }

            if (drive.EndsWith(":\\"))
            {
                //C:\ -> C
                drive = drive.Substring(0, drive.Length - 2);
            }

            string volumeSerial = getVolumeSerial(drive);
            string cpuID = getCPUID();

            //Mix them up and remove some useless 0's
            return cpuID.Substring(13) + cpuID.Substring(1, 4) + volumeSerial + cpuID.Substring(4, 4);
        }

        private static string getVolumeSerial(string drive)
        {
            ManagementObject disk = new ManagementObject(@"win32_logicaldisk.deviceid=""" + drive + @":""");
            disk.Get();

            string volumeSerial = disk["VolumeSerialNumber"].ToString();
            disk.Dispose();

            return volumeSerial;
        }

        private static string getCPUID()
        {
            string cpuInfo = "";
            ManagementClass managClass = new ManagementClass("win32_processor");
            ManagementObjectCollection managCollec = managClass.GetInstances();

            foreach (ManagementObject managObj in managCollec)
            {
                if (cpuInfo == "")
                {
                    //Get only the first CPU's ID
                    cpuInfo = managObj.Properties["processorID"].Value.ToString();
                    break;
                }
            }

            return cpuInfo;
        }

        internal void CheckIfAllowed()
        {
            string MacAddress = GetMacAddress();
            if (names.Contains(MacAddress))
            {
                //Console.WriteLine(">> MAC Valid Now Starting Mercury");
                string MachineId = getUniqueID("C");
                if (machines.Contains(MachineId))
                {
                    //Console.WriteLine(">> MAC Valid Now Starting Mercury");
                }
                else
                {
                    Environment.Exit(1);
                }
            }
            else
            {
                Environment.Exit(1);
            }
            //Console.WriteLine(MacAddress);
        }

        private static IEnumerable<IPAddress> GetIpAddress()
        {
            var host = Dns.GetHostEntry(Dns.GetHostName());
            return (from ip in host.AddressList where !IPAddress.IsLoopback(ip) select ip).ToList();
        }
    }
}