using ConnectionManager;
using System;
using System.Collections.Generic;
namespace Mercury.Messages
{
	public class QueuedServerMessage
	{
		private List<byte> packet;
		private ConnectionInformation userConnection;
		internal byte[] getPacket
		{
			get
			{
				return this.packet.ToArray();
			}
		}
		public QueuedServerMessage(ConnectionInformation connection)
		{
			this.userConnection = connection;
			this.packet = new List<byte>();
		}
		internal void Dispose()
		{
			this.packet.Clear();
			this.userConnection = null;
		}
		private void AppendByteds(byte[] bytes)
		{
			this.packet.AddRange(bytes);
		}
		internal void appendResponse(ServerMessage message)
		{
			this.AppendByteds(message.GetBytes());
		}
		internal void addBytes(byte[] bytes)
		{
			this.AppendByteds(bytes);
		}
		internal void sendResponse()
		{
			if (this.userConnection != null)
			{
				this.userConnection.SendMuchData(this.packet.ToArray());
			}
			this.Dispose();
		}
	}
}
