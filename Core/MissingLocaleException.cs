using System;
namespace Mercury.Core
{
	internal class MissingLocaleException : Exception
	{
		public MissingLocaleException(string message) : base(message)
		{
		}
	}
}
