namespace Mercury.Core
{
    using Database_Manager.Database.Session_Details.Interfaces;
    using Mercury;
    using Mercury.HabboHotel;
    using Mercury.Messages;
    using Mercury.Messages.Headers;
    using System;

    internal class ConsoleCommandHandling
    {
        internal static bool isWaiting = false;

        internal static Game getGame()
        {
            return MercuryEnvironment.GetGame();
        }

        internal static void InvokeCommand(string inputData)
        {
            if (string.IsNullOrEmpty(inputData) && Logging.DisabledState)
            {
                return;
            }
            Console.WriteLine();
            try
            {
                string[] strArray = inputData.Split(new char[] { ' ' });
                switch (strArray[0])
                {
                    case "shutdown":
                    case "apagar":
                        //Logging.LogMessage("Server shutting down at " + DateTime.Now);
                        Logging.DisablePrimaryWriting(true);
                        Logging.WriteLine("Attempting to shut down...", Mercury.ConsoleColor.Yellow);
                        MercuryEnvironment.PerformShutDown(false);
                        Console.WriteLine();
                        break;

                    case "flush":
                    case "refrescar":
                        if (strArray.Length >= 2)
                        {
                            break;
                        }
                        Console.WriteLine("Please specify parameter. Type 'help' to know more about Console Commands");
                        Console.WriteLine();
                        break;


                    case "alert":
                        {
                            string str = inputData.Substring(6);
                            ServerMessage message = new ServerMessage(Outgoing.BroadcastNotifMessageComposer);
                            message.AppendString(str);
                            message.AppendString("");
                            getGame().GetClientManager().QueueBroadcaseMessage(message);
                            Console.WriteLine("[" + str + "] was sent!");
                            break;
                        }

                    case "help":
                        {
                        Console.WriteLine("shutdown / apagar - for shutting down MercuryEmulator");
                        Console.WriteLine("flush / refrescar");
                        Console.WriteLine("      settings");
                        Console.WriteLine("           catalog - re-load Catalogue");
                        Console.WriteLine("alert (msg) - send alert to Every!");
                        Console.WriteLine("Version - Look at your emulator version");
                        Console.WriteLine();
                            break;
                      }

                   case "version":
                        Console.WriteLine("This version of Darklight is the White version");
                        Console.WriteLine("The white version is the Final('s) ");
                        Console.WriteLine("You also have the Purple and the Gray version");
                        Console.WriteLine("Purple = Alpha and Gray = Beta");


                           break; 

                    default:
                            unknownCommand(inputData);
                            break;
                }
                switch (strArray[1])
                {
                    case "database":
                        MercuryEnvironment.GetDatabaseManager().Destroy();
                        Console.WriteLine("Database destroyed");
                        Console.WriteLine();
                            break;

                    case "console":
                    case "consola":
                        Console.Clear();
                        Console.WriteLine();
                            break;

                    default:
                         unknownCommand(inputData);
                Console.WriteLine();
                break;
                }

                switch (strArray[2])
                {
                    case "catalog":
                    case "shop":
                    case "catalogo":
                        using (IQueryAdapter adapter = MercuryEnvironment.GetDatabaseManager().getQueryreactor())
                        {
                            getGame().GetCatalog().Initialize(adapter);
                        }
                        getGame().GetClientManager().QueueBroadcaseMessage(new ServerMessage(Outgoing.PublishShopMessageComposer));
                        Console.WriteLine("Catalogue was re-loaded.");
                        Console.WriteLine();
                            break;

                    case "modeldata":
                        using (IQueryAdapter adapter2 = MercuryEnvironment.GetDatabaseManager().getQueryreactor())
                        {
                            getGame().GetRoomManager().LoadModels(adapter2);
                        }
                        Console.WriteLine("Room models were re-loaded.");
                        Console.WriteLine();
                        break;

                    case "bans":
                        using (IQueryAdapter adapter3 = MercuryEnvironment.GetDatabaseManager().getQueryreactor())
                        {
                            getGame().GetBanManager().LoadBans(adapter3);
                        }
                        Console.WriteLine("Bans were re-loaded");
                        Console.WriteLine();
                        break;

                    default:
                        Console.WriteLine();
                        break;
                }
            }
            catch (Exception)
            {
            }
        }

        private static void unknownCommand(string command)
        {
        }
    }
}

