using System;
namespace Mercury.Util
{
	internal enum EndingType
	{
		None,
		Sequential,
		Continuous
	}
}
