using Database_Manager.Database.Session_Details.Interfaces;
using HabboEncryption;
using HabboEncryption.Keys;
using Mercury.Core;
using Mercury.Database;
using Mercury.HabboHotel;
using Mercury.HabboHotel.Catalogs;
using Mercury.HabboHotel.GameClients;
using Mercury.HabboHotel.Groups;
using Mercury.HabboHotel.Pets;
using Mercury.HabboHotel.Users;
using Mercury.HabboHotel.Users.Messenger;
using Mercury.HabboHotel.Users.UserDataManagement;
using Mercury.Messages;
using Mercury.Messages.Headers;
using Mercury.Messages.StaticMessageHandlers;
using Mercury.Net;
using Mercury.Util;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Diagnostics;
using System.Globalization;
using System.IO;
using System.Text;
using System.Threading;
using System.Windows.Forms;

namespace Mercury
{
	internal static class MercuryEnvironment
	{

		internal static readonly string PrettyBuild = "2.X";
		internal static readonly string PrettyVersion = "Darklight";
        internal static readonly string PrettyRelease = "RELEASE63-201408141029-609065162-ROCKedit";
        internal static readonly string PrettyVersionV = "White darklight (Final beta)";
		internal static bool isLive;
		internal static bool SeparatedTasksInGameClientManager = false;
		internal static bool SeparatedTasksInMainLoops = false;
		internal static ConfigData ConfigData;
		internal static CultureInfo cultureInfo;
		internal static DateTime ServerStarted;
		internal static Dictionary<uint, List<OfflineMessage>> OfflineMessages;
		internal static GiftWrappers GiftWrappers;
		internal static int LiveCurrencyType = 105;
		internal static MusSocket MusSystem;
		private static ConfigurationData Configuration;
		private static ConnectionHandling ConnectionManager;
		private static DatabaseManager manager;
		private static Encoding DefaultEncoding;
		private static Game Game;
		public static uint FriendRequestLimit = 1000;

        private static readonly HashSet<char> allowedchars = new HashSet<char>(new char[]
		{
			'a',
			'b',
			'c',
			'd',
			'e',
			'f',
			'g',
			'h',
			'i',
			'j',
			'k',
			'l',
			'm',
			'n',
			'o',
			'p',
			'q',
			'r',
			's',
			't',
			'u',
			'v',
			'w',
			'x',
			'y',
			'z',
			'1',
			'2',
			'3',
			'4',
			'5',
			'6',
			'7',
			'8',
			'9',
			'0',
			'-',
			'.',
			'á',
			'é',
			'í',
			'ó',
			'ú',
			'ñ',
			'Ñ',
			'ü',
			'Ü',
			'Á',
			'É',
			'Í',
			'Ó',
			'Ú',
			' ', 'Ã', '©', '¡', '­', 'º', '³','Ã','‰','_'
		});

		private static HybridDictionary usersCached = new HybridDictionary();
		private static bool ShutdownInitiated = false;
        internal static uint StaffAlertMinRank = 4;

		internal static bool ShutdownStarted
		{
			get
			{
				return MercuryEnvironment.ShutdownInitiated;
			}
		}
		internal static void Initialize()
		{
            // Check Security
            Console.Clear(); 
			DateTime now = DateTime.Now;
			MercuryEnvironment.ServerStarted = DateTime.Now;
            Console.BackgroundColor = System.ConsoleColor.Black;
            Console.ForegroundColor = System.ConsoleColor.White;
            Console.WindowWidth = 120;
            Console.WindowHeight = 36;
            Console.Clear();
            Console.WriteLine();
            Console.WriteLine(@"    ___           _    _ _       _     _       __  __  _____ _     ");
            Console.WriteLine(@"   /   \__ _ _ __| | _| (_) __ _| |__ | |_    /__\/ /_|___ /| |__  ");
            Console.WriteLine(@"  / /\ / _` | '__| |/ / | |/ _` | '_ \| __|  / \// '_ \ |_ \| '_ \ ");
            Console.WriteLine(@" / /_// (_| | |  |   <| | | (_| | | | | |_  / _  \ (_) |__) | |_) |");
            Console.WriteLine(@"/___,' \__,_|_|  |_|\_\_|_|\__, |_| |_|\__| \/ \_/\___/____/|_.__/ ");
            Console.WriteLine(@"                           |___/                                   ");
            Console.WriteLine("    Darklight emulator systems for R63B+ V1.0.1 (not emulator version btw)");
			Console.WriteLine();
            Console.WriteLine(@"                    Before Mecury v3.2                       Darklight v" + PrettyBuild);
            Console.WriteLine(@"                   Based on Plus. Name Rights To Rockster");
            Console.WriteLine(@"  System version '"+ PrettyVersionV +"' Made on 3-12-2014  ");//New here becuz i have white, purple and gray version
            Console.ForegroundColor = System.ConsoleColor.Red;
            Console.WriteLine(@"                                            " + PrettyRelease);
			Console.Title = "Darklight | Loading [...]";
			MercuryEnvironment.DefaultEncoding = Encoding.Default;
			Console.WriteLine("");
            Console.ResetColor();

            MercuryEnvironment.cultureInfo = CultureInfo.CreateSpecificCulture("en-GB");
            TextHandling.replaceDecimal();
                try
                {
                    MercuryEnvironment.Configuration = new ConfigurationData(Path.Combine(Application.StartupPath, "config.ini"), false);

                    MySqlConnectionStringBuilder mySqlConnectionStringBuilder = new MySqlConnectionStringBuilder();
                    mySqlConnectionStringBuilder.Server = (MercuryEnvironment.GetConfig().data["db.hostname"]);
                    mySqlConnectionStringBuilder.Port = (uint.Parse(MercuryEnvironment.GetConfig().data["db.port"]));
                    mySqlConnectionStringBuilder.UserID = (MercuryEnvironment.GetConfig().data["db.username"]);
                    mySqlConnectionStringBuilder.Password = (MercuryEnvironment.GetConfig().data["db.password"]);
                    mySqlConnectionStringBuilder.Database = (MercuryEnvironment.GetConfig().data["db.name"]);
                    mySqlConnectionStringBuilder.MinimumPoolSize = 1;
                    mySqlConnectionStringBuilder.MaximumPoolSize = 9000;
                    mySqlConnectionStringBuilder.Pooling = (true);
                    mySqlConnectionStringBuilder.AllowZeroDateTime = (true);
                    mySqlConnectionStringBuilder.ConvertZeroDateTime = (true);
                    mySqlConnectionStringBuilder.DefaultCommandTimeout = (3000u);
                    mySqlConnectionStringBuilder.ConnectionTimeout = (100u);
                    MySqlConnectionStringBuilder mySqlConnectionStringBuilder2 = mySqlConnectionStringBuilder;
                    MercuryEnvironment.manager = new DatabaseManager(mySqlConnectionStringBuilder2.ToString());

                    using (IQueryAdapter queryreactor = MercuryEnvironment.GetDatabaseManager().getQueryreactor())
                    {
                        ConfigData = new ConfigData(queryreactor);
                        PetLocale.Init(queryreactor);
                        OfflineMessages = new Dictionary<uint, List<OfflineMessage>>();
                        OfflineMessage.InitOfflineMessages(queryreactor);
                        GiftWrappers = new GiftWrappers(queryreactor);
                    }

                    FriendRequestLimit = checked((uint)int.Parse(MercuryEnvironment.GetConfig().data["client.maxrequests"]));

                    Game = new Game(int.Parse(GetConfig().data["game.tcp.conlimit"]));
                    Game.ContinueLoading();
                    if (ExtraSettings.RunExtraSettings())
                    {
                        Logging.WriteLine("Loaded an Extra Settings file.", ConsoleColor.Red);
                        Console.ResetColor();
                    }

                    ConnectionManager = new ConnectionHandling(int.Parse(GetConfig().data["game.tcp.port"]), int.Parse(GetConfig().data["game.tcp.conlimit"]), int.Parse(GetConfig().data["game.tcp.conperip"]), GetConfig().data["game.tcp.enablenagles"].ToLower() == "true");

                    HabboCrypto.Initialize(new RsaKeyHolder());
                    MercuryEnvironment.ConnectionManager.init();
                    MercuryEnvironment.ConnectionManager.Start();
                    StaticClientMessageHandler.Initialize();

                    string[] allowedIps = GetConfig().data["mus.tcp.allowedaddr"].Split(';');
                    MusSystem = new MusSocket(GetConfig().data["mus.tcp.bindip"], int.Parse(GetConfig().data["mus.tcp.port"]), allowedIps, 0);


                    if (Configuration.data.ContainsKey("StaffAlert.MinRank"))
                    {
                        StaffAlertMinRank = uint.Parse(MercuryEnvironment.GetConfig().data["StaffAlert.MinRank"]);
                    }
                    if (Configuration.data.ContainsKey("SeparatedTasksInMainLoops.enabled") && Configuration.data["SeparatedTasksInMainLoops.enabled"] == "true")
                    {
                        SeparatedTasksInMainLoops = true;
                    }
                    if (Configuration.data.ContainsKey("SeparatedTasksInGameClientManager.enabled") && Configuration.data["SeparatedTasksInGameClientManager.enabled"] == "true")
                    {
                        SeparatedTasksInGameClientManager = true;
                    }

                    isLive = true;
                }
                catch (KeyNotFoundException)
                {
                    Logging.WriteLine("Something is missing in your configuration", ConsoleColor.Red);
                    Logging.WriteLine("Please type a key to shut down ...", ConsoleColor.Red);
                    Console.ReadKey(true);
                    MercuryEnvironment.Destroy();
                }
                catch (InvalidOperationException ex)
                {
                    Logging.WriteLine("Something wrong happened: " + ex.Message, ConsoleColor.Red);
                    Logging.WriteLine("Please type a key to shut down...", ConsoleColor.Red);
                    Console.ReadKey(true);
                    MercuryEnvironment.Destroy();
                }
                catch (Exception ex2)
                {
                    Logging.WriteLine("An exception got caught: " + ex2.Message, ConsoleColor.Red);
                    Logging.WriteLine("Type a key to know more about the error", ConsoleColor.Red);
                    Console.ReadKey();
                    Logging.WriteLine(ex2.ToString(), ConsoleColor.Yellow);
                    Logging.WriteLine("Please type a ket to shut down...", ConsoleColor.Red);
                    Console.ReadKey();
                    Environment.Exit(1);
                }
		}
		internal static bool EnumToBool(string Enum)
		{
			return Enum == "1";
		}

		internal static int BoolToInteger(bool Bool)
		{
			if (Bool)
			{
				return 1;
			}
			return 0;
		}
		internal static string BoolToEnum(bool Bool)
		{
			if (Bool)
			{
				return "1";
			}
			return "0";
		}
		internal static int GetRandomNumber(int Min, int Max)
		{
			return RandomNumber.GenerateNewRandom(Min, Max);
		}
		internal static int GetUnixTimestamp()
		{
			double totalSeconds = (DateTime.UtcNow - new DateTime(1970, 1, 1, 0, 0, 0)).TotalSeconds;
			return checked((int)totalSeconds);
		}
		internal static DateTime UnixToDateTime(double unixTimeStamp)
		{
			DateTime result = new DateTime(1970, 1, 1, 0, 0, 0, 0);
			result = result.AddSeconds(unixTimeStamp).ToLocalTime();
			return result;
		}
		internal static int DateTimeToUnix(DateTime target)
		{
			DateTime d = new DateTime(1970, 1, 1, 0, 0, 0, target.Kind);
			return Convert.ToInt32((target - d).TotalSeconds);
		}
		internal static long Now()
		{
			double totalMilliseconds = (DateTime.UtcNow - new DateTime(1970, 1, 1, 0, 0, 0)).TotalMilliseconds;
			return checked((long)totalMilliseconds);
		}
		internal static string FilterFigure(string figure)
		{
			for (int i = 0; i < figure.Length; i++)
			{
				char character = figure[i];
				if (!MercuryEnvironment.isValid(character))
				{
					return "lg-3023-1335.hr-828-45.sh-295-1332.hd-180-4.ea-3168-89.ca-1813-62.ch-235-1332";
				}
			}
			return figure;
		}
		private static bool isValid(char character)
		{
			return MercuryEnvironment.allowedchars.Contains(character);
		}
		internal static bool IsValidAlphaNumeric(string inputStr)
		{
			inputStr = inputStr.ToLower();
			if (string.IsNullOrEmpty(inputStr))
			{
				return false;
			}
			checked
			{
				for (int i = 0; i < inputStr.Length; i++)
				{
					if (!MercuryEnvironment.isValid(inputStr[i]))
					{
						return false;
					}
				}
				return true;
			}
		}
		internal static Habbo getHabboForId(uint UserId)
		{
			Habbo result;
			try
			{
				GameClient clientByUserID = MercuryEnvironment.GetGame().GetClientManager().GetClientByUserID(UserId);
				if (clientByUserID != null)
				{
					Habbo habbo = clientByUserID.GetHabbo();
					if (habbo != null && habbo.Id > 0u)
					{
						if (MercuryEnvironment.usersCached.Contains(UserId))
						{
							MercuryEnvironment.usersCached.Remove(UserId);
						}
						result = habbo;
						return result;
					}
				}
				else
				{
					if (MercuryEnvironment.usersCached.Contains(UserId))
					{
						result = (Habbo)MercuryEnvironment.usersCached[UserId];
						return result;
					}
					UserData userData = UserDataFactory.GetUserData(checked((int)UserId));
					Habbo user = userData.user;
					if (user != null)
					{
						user.InitInformation(userData);
						MercuryEnvironment.usersCached.Add(UserId, user);
						result = user;
						return result;
					}
				}
				result = null;
			}
			catch
			{
				result = null;
			}
			return result;
		}
		internal static Habbo getHabboForName(string UserName)
		{
			Habbo result;
			try
			{
				using (IQueryAdapter queryreactor = MercuryEnvironment.GetDatabaseManager().getQueryreactor())
				{
					queryreactor.setQuery("SELECT id FROM users WHERE username = @user");
					queryreactor.addParameter("user", UserName);
					int integer = queryreactor.getInteger();
					if (integer > 0)
					{
						result = MercuryEnvironment.getHabboForId(checked((uint)integer));
						return result;
					}
				}
				result = null;
			}
			catch
			{
				result = null;
			}
			return result;
		}
		internal static bool IsNum(string Int)
		{
			double num;
			return double.TryParse(Int, out num);
		}
		internal static ConfigurationData GetConfig()
		{
			return MercuryEnvironment.Configuration;
		}
		internal static ConfigData GetDBConfig()
		{
			return MercuryEnvironment.ConfigData;
		}
		internal static Encoding GetDefaultEncoding()
		{
			return MercuryEnvironment.DefaultEncoding;
		}
		internal static ConnectionHandling GetConnectionManager()
		{
			return MercuryEnvironment.ConnectionManager;
		}
		internal static Game GetGame()
		{
			return MercuryEnvironment.Game;
		}
		internal static void Destroy()
		{
			MercuryEnvironment.isLive = false;
			Logging.WriteLine("Destroying MercuryEnvironment...", ConsoleColor.Red);
			if (MercuryEnvironment.GetGame() != null)
			{
				MercuryEnvironment.GetGame().Destroy();
                MercuryEnvironment.GetGame().GetPixelManager().Destroy();
				MercuryEnvironment.Game = null;
			}
			if (MercuryEnvironment.GetConnectionManager() != null)
			{
				Logging.WriteLine("Destroying ConnectionManager...", ConsoleColor.Red);
				MercuryEnvironment.GetConnectionManager().Destroy();
			}
			if (MercuryEnvironment.manager != null)
			{
				try
				{
					Logging.WriteLine("Destroying DatabaseManager...", ConsoleColor.Red);
					MercuryEnvironment.manager.Destroy();
				}
				catch
				{
				}
			}
			Logging.WriteLine("Closing...", ConsoleColor.Red);
			Thread.Sleep(500);
			Environment.Exit(1);
		}
		internal static void SendMassMessage(string Message)
		{
			try
			{
				ServerMessage serverMessage = new ServerMessage(Outgoing.BroadcastNotifMessageComposer);
				serverMessage.AppendString(Message);
				MercuryEnvironment.GetGame().GetClientManager().QueueBroadcaseMessage(serverMessage);
			}
			catch (Exception pException)
			{
				Logging.HandleException(pException, "MercuryEnvironment.SendMassMessage");
			}
		}
		internal static string FilterInjectionChars(string Input)
		{
			Input = Input.Replace('\u0001', ' ');
			Input = Input.Replace('\u0002', ' ');
			Input = Input.Replace('\u0003', ' ');
			Input = Input.Replace('\t', ' ');
			return Input;
		}
		internal static DatabaseManager GetDatabaseManager()
		{
			return MercuryEnvironment.manager;
		}
		internal static void PerformShutDown()
		{
			MercuryEnvironment.PerformShutDown(false);
		}
		internal static void PerformShutDown(bool Restart)
		{
			//Console.Clear();
			DateTime now = DateTime.Now;
			MercuryEnvironment.ShutdownInitiated = true;
			ServerMessage serverMessage = new ServerMessage(Outgoing.SuperNotificationMessageComposer);
			serverMessage.AppendString("disconnection");
			serverMessage.AppendInt32(2);
			serverMessage.AppendString("title");
			serverMessage.AppendString("Hallo iedereen!");
			serverMessage.AppendString("message");
			if (Restart)
			{
				serverMessage.AppendString("<b>Het hotel gaat even rust houden.</b>\nDus misschien tot zo.\r\n<b>Bye bye!</b>");
			}
			else
			{
                serverMessage.AppendString("<b>Het hotel is even eten halen.</b><br />Wees maar niet bang alles word opgeslagen in Database<br /><b>Bye bye!</b>\r\n~ Deze sessie is medemogelijk gemaakt door Darklight en Rockster");
			}
			MercuryEnvironment.GetGame().GetClientManager().QueueBroadcaseMessage(serverMessage);
			Thread.Sleep(6000);
			MercuryEnvironment.Game.StopGameLoop();
			DateTime arg_93_0 = DateTime.Now;
			Logging.WriteLine("Shutting down...", ConsoleColor.Yellow);
			Console.Title = "Darklight Emulator | Shutting down...";
			DateTime arg_AF_0 = DateTime.Now;
			MercuryEnvironment.GetGame().GetClientManager().CloseAll();
			DateTime arg_C4_0 = DateTime.Now;
            MercuryEnvironment.Game.GetRoomManager().RemoveAllRooms();
            foreach (Guild Group in Game.GetGroupManager().Groups.Values)
            {
                Group.UpdateForum();
            }


			MercuryEnvironment.GetConnectionManager().Destroy();
			DateTime arg_E3_0 = DateTime.Now;
			using (IQueryAdapter queryreactor = MercuryEnvironment.manager.getQueryreactor())
			{
				queryreactor.runFastQuery("UPDATE users SET online = '0'");
				queryreactor.runFastQuery("UPDATE rooms SET users_now = 0");
				queryreactor.runFastQuery("TRUNCATE TABLE user_roomvisits");
			}
			DateTime arg_121_0 = DateTime.Now;
			MercuryEnvironment.ConnectionManager.Destroy();
			DateTime arg_131_0 = DateTime.Now;
			MercuryEnvironment.Game.Destroy();
			DateTime arg_141_0 = DateTime.Now;
			try
			{
				Console.WriteLine("Destroying database manager...");
				MercuryEnvironment.manager.Destroy();
			}
			catch
			{
			}
			TimeSpan span = DateTime.Now - now;
			Console.WriteLine("Darklight Emulator took " + MercuryEnvironment.TimeSpanToString(span) + " in shutdown process.");
			Console.WriteLine("Darklight Emulator has shut down succesfully.");
			MercuryEnvironment.isLive = false;
			if (Restart)
			{
				Process.Start(AppDomain.CurrentDomain.BaseDirectory + "\\Mercury Emulator.exe");
			}
            Console.WriteLine("Closing...");
			Environment.Exit(0);
		}
		internal static string TimeSpanToString(TimeSpan span)
		{
			return string.Concat(new object[]
			{
				span.Seconds,
				" s, ",
				span.Milliseconds,
				" ms"
			});
		}

	}
}
