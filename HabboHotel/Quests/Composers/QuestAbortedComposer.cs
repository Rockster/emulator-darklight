using Mercury.Messages;
using Mercury.Messages.Headers;
using System;
namespace Mercury.HabboHotel.Quests.Composer
{
	internal class QuestAbortedComposer
	{
		internal static ServerMessage Compose()
		{
			ServerMessage serverMessage = new ServerMessage(Outgoing.QuestAbortedMessageComposer);
			serverMessage.AppendBoolean(false);
			return serverMessage;
		}
	}
}
