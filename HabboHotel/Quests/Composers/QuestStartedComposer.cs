using Mercury.HabboHotel.GameClients;
using Mercury.Messages;
using Mercury.Messages.Headers;
using System;
namespace Mercury.HabboHotel.Quests.Composer
{
	internal class QuestStartedComposer
	{
		internal static ServerMessage Compose(GameClient Session, Quest Quest)
		{
			ServerMessage serverMessage = new ServerMessage(Outgoing.QuestStartedMessageComposer);
			QuestListComposer.SerializeQuest(serverMessage, Session, Quest, Quest.Category);
			return serverMessage;
		}
	}
}
