using System;
namespace Mercury.HabboHotel.Navigators
{
	internal enum PublicItemType
	{
		NONE,
		TAG,
		FLAT,
		PUBLIC_FLAT,
		CATEGORY
	}
}
