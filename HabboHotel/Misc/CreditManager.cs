using Mercury.HabboHotel.GameClients;
using System;
namespace Mercury.HabboHotel.Misc
{
	internal class CreditManager
	{
		internal static void GiveCredits(GameClient Client, int amount)
		{
			if (Client == null || Client.GetHabbo() == null)
			{
				return;
			}
			double arg_12_0 = (double)MercuryEnvironment.GetUnixTimestamp();
			checked
			{
				Client.GetHabbo().Credits += amount;
				Client.GetHabbo().UpdateCreditsBalance();
			}
		}
	}
}
