using Database_Manager.Database.Session_Details.Interfaces;
using Mercury.Core;
using System;
using System.Diagnostics;
using System.Net;
using System.Threading;

namespace Mercury.HabboHotel.Misc
{
    internal class LowPriorityWorker
    {
        private static int UserPeak;
        private static Timer mTimer;

        internal static void Init(IQueryAdapter dbClient)
        {
            dbClient.setQuery("SELECT userpeak FROM server_status");
            LowPriorityWorker.UserPeak = dbClient.getInteger();

        }

        internal static void StartProcessing()
        {
            mTimer = new Timer(new TimerCallback(Process), null, 0, 60000);
        }

        internal static void Process(object Caller)
        {
            int clientCount = MercuryEnvironment.GetGame().GetClientManager().ClientCount;
            int loadedRoomsCount = MercuryEnvironment.GetGame().GetRoomManager().LoadedRoomsCount;
            DateTime dateTime = new DateTime((DateTime.Now - MercuryEnvironment.ServerStarted).Ticks);
            string text = dateTime.ToString("HH:mm:ss");


            Console.Title = string.Concat(new object[]
						{
							"Darklight v", MercuryEnvironment.PrettyBuild,  " | TIME: ",
							text,
							" | ONLINE COUNT: ",
							clientCount,
							" | ROOM COUNT: ",
							loadedRoomsCount
						});

            if (clientCount > LowPriorityWorker.UserPeak)
            {
                LowPriorityWorker.UserPeak = clientCount;
            }
            using (IQueryAdapter queryreactor = MercuryEnvironment.GetDatabaseManager().getQueryreactor())
            {
                queryreactor.runFastQuery(string.Concat(new object[]
							{
								"UPDATE server_status SET stamp = '",
								MercuryEnvironment.GetUnixTimestamp(),
								"', users_online = ",
								clientCount,
								", rooms_loaded = ",
								loadedRoomsCount,
								", server_ver = '"+ MercuryEnvironment.PrettyVersionV +"', userpeak = ",
								LowPriorityWorker.UserPeak
							}));
            }
        }
    }
}
