using System;
using System.Collections;
namespace Mercury.HabboHotel.Rooms.Wired
{
	public interface WiredCycler
	{
		Queue ToWork
		{
			get;
			set;
		}
		bool OnCycle();
	}
}
