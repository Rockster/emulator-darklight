using System;
namespace Mercury.HabboHotel.Rooms.Games
{
	internal enum ComeDirection
	{
		UP,
		UP_RIGHT,
		RIGHT,
		DOWN_RIGHT,
		DOWN,
		DOWN_LEFT,
		LEFT,
		UP_LEFT,
		NULL
	}
}
