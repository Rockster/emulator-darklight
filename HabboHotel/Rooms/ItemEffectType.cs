using System;
namespace Mercury.HabboHotel.Rooms
{
	internal enum ItemEffectType
	{
		None,
		Swim,
		SwimLow,
		SwimHalloween,
		Iceskates,
		Normalskates,
		PublicPool,
        SnowBoard
	}
}
