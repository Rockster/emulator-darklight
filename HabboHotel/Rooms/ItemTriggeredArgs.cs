using Mercury.HabboHotel.Items;
using System;
namespace Mercury.HabboHotel.Rooms
{
	public class ItemTriggeredArgs : EventArgs
	{
		internal readonly RoomUser TriggeringUser;
		internal readonly RoomItem TriggeringItem;
		public ItemTriggeredArgs(RoomUser user, RoomItem item)
		{
			this.TriggeringUser = user;
			this.TriggeringItem = item;
		}
	}
}
