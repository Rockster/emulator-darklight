using System;
namespace Mercury.HabboHotel.Rooms
{
	internal enum SquareState
	{
		OPEN,
		BLOCKED,
		SEAT,
		POOL,
		VIP
	}
}
