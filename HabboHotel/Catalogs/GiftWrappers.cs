using Database_Manager.Database.Session_Details.Interfaces;
using Mercury.Core;
using System;
using System.Collections.Generic;
using System.Data;
namespace Mercury.HabboHotel.Catalogs
{
	internal class GiftWrappers
	{
		internal List<uint> GiftWrappersList = new List<uint>();
        internal List<uint> OldGiftWrappers = new List<uint>();

		internal GiftWrappers(IQueryAdapter dbClient)
		{
			dbClient.setQuery("SELECT * FROM gift_wrappers");
			DataTable table = dbClient.getTable();
            if (table == null)
            { return; }
			foreach (DataRow dataRow in table.Rows)
			{
                if (dataRow["type"].ToString() == "new")
                {
                    this.GiftWrappersList.Add((uint)dataRow["sprite_id"]);
                }
                else if (dataRow["type"].ToString() == "old")
                {
                    this.OldGiftWrappers.Add((uint)dataRow["sprite_id"]);
                }
			}
		}
	}
}
