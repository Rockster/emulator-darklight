using System;
using System.Threading;
using Database_Manager.Database.Session_Details.Interfaces;
using Mercury.Core;
using Mercury.HabboHotel.Achievements;
using Mercury.HabboHotel.Catalogs;
using Mercury.HabboHotel.GameClients;
using Mercury.HabboHotel.Groups;
using Mercury.HabboHotel.Items;
using Mercury.HabboHotel.Misc;
using Mercury.HabboHotel.Navigators;
using Mercury.HabboHotel.Pets;
using Mercury.HabboHotel.Polls;
using Mercury.HabboHotel.Quests;
using Mercury.HabboHotel.Roles;
using Mercury.HabboHotel.RoomBots;
using Mercury.HabboHotel.Rooms;
using Mercury.HabboHotel.SoundMachine;
using Mercury.HabboHotel.Support;
using Mercury.HabboHotel.Users.Inventory;
using Mercury.HabboHotel.YouTube;

namespace Mercury.HabboHotel
{
    internal class Game
    {
        internal static bool gameLoopEnabled = true;
        internal bool ClientManagerCycle_ended;

        internal bool RoomManagerCycle_ended;
        private readonly GameClientManager ClientManager;
        private readonly ModerationBanManager BanManager;
        private readonly RoleManager RoleManager;
        private readonly Catalog Catalog;
        private readonly Navigator Navigator;
        private readonly ItemManager ItemManager;
        private readonly RoomManager RoomManager;
        private readonly HotelView HotelView;
        private readonly PixelManager PixelManager;
        private readonly AchievementManager AchievementManager;
        private readonly ModerationTool ModerationTool;
        private readonly BotManager BotManager;
        private readonly QuestManager questManager;
        private readonly GroupManager groupManager;
        private readonly RoomEvents Events;
        private readonly TalentManager talentManager;
        private readonly VideoManager VideoManager;
        private readonly PinataHandler PinataHandler;
        private readonly PollManager PollManager;
        private InventoryGlobal globalInventory;
        private Thread ConsoleLoop;
        private AntiMutant AntiMutant;
        private Thread gameLoop;

        internal Game(int conns)
        {
            this.ClientManager = new GameClientManager();
            using (IQueryAdapter queryreactor = MercuryEnvironment.GetDatabaseManager().getQueryreactor())
            {
                AbstractBar bar;
                bar = new AnimatedBar();
                int wait = 0;
                int end = 1;
                uint itemsLoaded = 0;
                uint navigatorLoaded = 0;
                uint roomModelLoaded = 0;
                uint achievementLoaded = 0;
                uint pollLoaded = 0;
                DateTime now = DateTime.Now;
                Console.ForegroundColor = System.ConsoleColor.DarkCyan;
                Console.WriteLine("≫ MAC Address Valid, Welcome to Mercury...");
                Console.WriteLine("≫ Loading MercuryEmulator...");
                WinConsole.Color = Mercury.ConsoleColor.Cyan;
                Progress(bar, wait, end, "Loading Bans...");
                this.BanManager = new ModerationBanManager();
                this.BanManager.LoadBans(queryreactor);
                Progress(bar, wait, end, "Loading Roles...");
                this.RoleManager = new RoleManager();
                this.RoleManager.LoadRights(queryreactor);
                Progress(bar, wait, end, "Loading Navigator...");
                this.Navigator = new Navigator();
                this.Navigator.Initialize(queryreactor, out navigatorLoaded);
                Progress(bar, wait, end, "Loading Items...");
                this.ItemManager = new ItemManager();
                this.ItemManager.LoadItems(queryreactor, out itemsLoaded);
                Progress(bar, wait, end, "Loading Catalog...");
                this.Catalog = new Catalog();
                Progress(bar, wait, end, "Loading Rooms...");
                this.RoomManager = new RoomManager();
                Progress(bar, wait, end, "Loading Groups...");
                this.groupManager = new GroupManager();
                this.GetGroupManager().InitGroups();
                this.RoomManager.LoadModels(queryreactor, out roomModelLoaded);
                this.globalInventory = new InventoryGlobal();
                Progress(bar, wait, end, "Loading PixelManager...");
                this.PixelManager = new PixelManager();
                Progress(bar, wait, end, "Loading HotelView...");
                this.HotelView = new HotelView();
                Progress(bar, wait, end, "Loading ModerationTool...");
                this.ModerationTool = new ModerationTool();
                this.ModerationTool.LoadMessagePresets(queryreactor);
                this.ModerationTool.LoadPendingTickets(queryreactor);
                Progress(bar, wait, end, "Loading Bots...");
                this.BotManager = new BotManager();
                Progress(bar, wait, end, "Loading Quests...");
                this.questManager = new QuestManager();
                this.questManager.Initialize(queryreactor);
                Progress(bar, wait, end, "Loading Events...");
                this.Events = new RoomEvents();
                Progress(bar, wait, end, "Loading Talents...");
                this.talentManager = new TalentManager();
                this.talentManager.Initialize(queryreactor);
                Progress(bar, wait, end, "Loading Videos...");
                this.VideoManager = new VideoManager();
                //this.SnowStormManager = new SnowStormManager();
                Progress(bar, wait, end, "Loading Pinata...");
                this.PinataHandler = new PinataHandler();
                this.PinataHandler.Initialize(queryreactor);
                Progress(bar, wait, end, "Loading Polls...");
                this.PollManager = new PollManager();
                this.PollManager.Init(queryreactor, out pollLoaded);
                Progress(bar, wait, end, "Loading Achievements...");
                this.AchievementManager = new AchievementManager(queryreactor, out achievementLoaded);
                //Progress(bar, wait, end, "Loading AntiMutant...");
                this.AntiMutant = new AntiMutant();
                Console.Write("\r".PadLeft(Console.WindowWidth - Console.CursorLeft - 1));
                WinConsole.Color = Mercury.ConsoleColor.Yellow;
                Console.WriteLine("≫ Yeap, Now Load Definitions...");
                Console.WriteLine("≫ New power 2014 Darklight White version...");
                Console.WriteLine("≫ Going on with loading...");
                Console.WriteLine();
                WinConsole.Color = Mercury.ConsoleColor.Cyan;
            }
        }

        internal bool gameLoopEnabled_EXT
        {
            get
            {
                return Game.gameLoopEnabled;
            }
        }

        internal bool gameLoopActive_EXT { get; private set; }

        internal int gameLoopSleepTime_EXT
        {
            get
            {
                return 25;
            }
        }

        internal AntiMutant GetAntiMutant()
        {
        return this.AntiMutant;
        }

        public static void Progress(AbstractBar bar, int wait, int end, string message)
        {
            bar.PrintMessage(message);
            for (int cont = 0; cont < end; cont++)
            {
                bar.Step();
                Thread.Sleep(wait);
            }
        }

        internal static void DatabaseCleanup(IQueryAdapter dbClient)
        {
            dbClient.runFastQuery("UPDATE users SET online = '0' WHERE online <> '0'");
            dbClient.runFastQuery("UPDATE rooms SET users_now = 0 WHERE users_now <> 0");
            dbClient.runFastQuery(string.Format("UPDATE server_status SET status = 1, users_online = 0, rooms_loaded = 0, server_ver = '" + MercuryEnvironment.PrettyVersionV + "', stamp = '{0}' ", MercuryEnvironment.GetUnixTimestamp()));
        }

        internal GameClientManager GetClientManager()
        {
            return this.ClientManager;
        }

        internal ModerationBanManager GetBanManager()
        {
            return this.BanManager;
        }

        internal RoleManager GetRoleManager()
        {
            return this.RoleManager;
        }

        internal Catalog GetCatalog()
        {
            return this.Catalog;
        }

        internal VideoManager GetVideoManager()
        {
            return this.VideoManager;
        }

        internal RoomEvents GetRoomEvents()
        {
            return this.Events;
        }

        internal Navigator GetNavigator()
        {
            return this.Navigator;
        }

        internal ItemManager GetItemManager()
        {
            return this.ItemManager;
        }

        internal RoomManager GetRoomManager()
        {
            return this.RoomManager;
        }

        internal PixelManager GetPixelManager()
        {
            return this.PixelManager;
        }

        internal HotelView GetHotelView()
        {
            return this.HotelView;
        }

        internal AchievementManager GetAchievementManager()
        {
            return this.AchievementManager;
        }

        internal ModerationTool GetModerationTool()
        {
            return this.ModerationTool;
        }

        internal BotManager GetBotManager()
        {
            return this.BotManager;
        }

        internal InventoryGlobal GetInventory()
        {
            return this.globalInventory;
        }

        internal QuestManager GetQuestManager()
        {
            return this.questManager;
        }

        internal GroupManager GetGroupManager()
        {
            return this.groupManager;
        }

        internal TalentManager GetTalentManager()
        {
            return this.talentManager;
        }

        internal PinataHandler GetPinataHandler()
        {
            return this.PinataHandler;
        }

        internal PollManager GetPollManager()
        {
            return this.PollManager;
        }

        internal void ContinueLoading()
        {
            using (IQueryAdapter queryreactor = MercuryEnvironment.GetDatabaseManager().getQueryreactor())
            {
                uint catalogPageLoaded = 0;
                PetRace.Init(queryreactor);
                this.Catalog.Initialize(queryreactor, out catalogPageLoaded);
                AntiPublicistas.Load(queryreactor);
                SongManager.Initialize();
                LowPriorityWorker.Init(queryreactor);
                this.RoomManager.InitVotedRooms(queryreactor);
            }
            this.StartGameLoop();
            this.PixelManager.StartPixelTimer();
        }

        internal void StartGameLoop()
        {
            this.gameLoopActive_EXT = true;
            this.gameLoop = new Thread(new ThreadStart(this.MainGameLoop));
            this.gameLoop.Start();
            this.ConsoleLoop = new Thread(new ThreadStart(this.ConsoleGoing));
            this.ConsoleLoop.Start();
        }

        internal void StopGameLoop()
        {
            this.gameLoopActive_EXT = false;
            while (!this.RoomManagerCycle_ended || !this.ClientManagerCycle_ended)
            {
                Thread.Sleep(25);
            }
        }

        internal void VideoGo()
        {
            using (IQueryAdapter queryreactor = MercuryEnvironment.GetDatabaseManager().getQueryreactor())
            {
                uint videoPlaylistLoaded = 0;
                this.VideoManager.Load(queryreactor, out videoPlaylistLoaded);
            }
        }

        internal void Destroy()
        {
            using (IQueryAdapter queryreactor = MercuryEnvironment.GetDatabaseManager().getQueryreactor())
            {
                Game.DatabaseCleanup(queryreactor);
            }
            this.GetClientManager();
            Console.WriteLine("Habbo Hotel was destroyed.");
        }

        internal void reloaditems()
        {
            using (IQueryAdapter queryreactor = MercuryEnvironment.GetDatabaseManager().getQueryreactor())
            {
                this.ItemManager.LoadItems(queryreactor);
                this.globalInventory = new InventoryGlobal();
            }
        }

        private void ConsoleGoing()
        {
            while (this.gameLoopActive_EXT)
            {
                if (Console.CursorTop >= 38)
                {
                    /*Console.Clear();
                    Console.SetCursorPosition(0, 0);
                    Console.ForegroundColor = System.ConsoleColor.DarkCyan;
                    Console.WriteLine();
                    Console.WriteLine(@"              _______                                    _______                  __         __              ");
                    Console.WriteLine(@"             |   |   |.-----.----.----.--.--.----.--.--.|    ___|.--------.--.--.|  |.---.-.|  |_.-----.----.");
                    Console.WriteLine(@"             |       ||  -__|   _|  __|  |  |   _|  |  ||    ___||        |  |  ||  ||  _  ||   _|  _  |   _|");
                    Console.WriteLine(@"             |__|_|__||_____|__| |____|_____|__| |___  ||_______||__|__|__|_____||__||___._||____|_____|__|  ");
                    Console.WriteLine(@"                                                 |_____|                                    ");
                    Console.WriteLine();
                    Console.WriteLine("                                                Mercury Emulator v" + MercuryEnvironment.PrettyBuild);
                    Console.WriteLine("                                      Developed by XDR, Ihton, Antoine, bi0s and Trylix");
                    Console.ForegroundColor = System.ConsoleColor.DarkYellow;
                    Console.WriteLine("------------------------------------------------------------------------------------------------------------------------");
                    Console.WriteLine();*/
                }
            }
        }

        private void MainGameLoop()
        {
            LowPriorityWorker.StartProcessing();

            while (this.gameLoopActive_EXT)
            {
                if (Game.gameLoopEnabled)
                {
                    try
                    {
                        this.RoomManagerCycle_ended = false;
                        this.ClientManagerCycle_ended = false;
                        this.RoomManager.OnCycle();
                        this.ClientManager.OnCycle();
                    }
                    catch (Exception ex)
                    {
                        Logging.LogCriticalException(string.Format("Exception in Game Loop!: {0}", ex.ToString()));
                    }
                }
                Thread.Sleep(25);
            }
        }
    }
}
