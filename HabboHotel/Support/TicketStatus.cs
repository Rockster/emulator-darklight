using System;
namespace Mercury.HabboHotel.Support
{
	internal enum TicketStatus
	{
		OPEN,
		PICKED,
		RESOLVED,
		ABUSIVE,
		INVALID,
		DELETED
	}
}
