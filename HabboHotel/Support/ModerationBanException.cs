using System;
namespace Mercury.HabboHotel.Support
{
	[Serializable]
	public class ModerationBanException : Exception
	{
		internal ModerationBanException(string Reason) : base(Reason)
		{
		}
	}
}
