using System;
namespace Mercury.HabboHotel.Pets
{
	internal enum DatabaseUpdateState
	{
		Updated,
		NeedsUpdate,
		NeedsInsert
	}
}
