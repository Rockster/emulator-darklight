using System;
namespace Mercury.HabboHotel.Pets
{
	internal enum MoplaState
	{
		ALIVE = 0,
        GROWN = 1,
		DEAD = 2
	}
}
