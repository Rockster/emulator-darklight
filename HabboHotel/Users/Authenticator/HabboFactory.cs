using Mercury.HabboHotel.Groups;
using System;
using System.Collections.Generic;
using System.Data;
using Mercury.HabboHotel.Users.UserDataManagement;

namespace Mercury.HabboHotel.Users.Authenticator
{
	internal static class HabboFactory
	{
		internal static Habbo GenerateHabbo(DataRow dRow, DataRow mRow, HashSet<GroupUser> group)
		{
			uint id = uint.Parse(dRow["id"].ToString());
			string username = (string)dRow["username"];
			string realName = (string)dRow["real_name"];
			uint ras = uint.Parse(dRow["rank"].ToString());
			string motto = (string)dRow["motto"];
			string look = (string)dRow["look"];
			string gender = (string)dRow["gender"];
			int lastOnline = int.Parse(dRow["last_online"].ToString());
			int credits = (int)dRow["credits"];
			int activityPoints = (int)dRow["activity_points"];
			double lastActivityPointsUpdate = Convert.ToDouble(dRow["activity_points_lastupdate"]);
			bool muted = MercuryEnvironment.EnumToBool(dRow["is_muted"].ToString());
			uint homeRoom = Convert.ToUInt32(dRow["home_room"]);
			int respect = (int)mRow["respect"];
			int dailyRespectPoints = (int)mRow["daily_respect_points"];
			int dailyPetRespectPoints = (int)mRow["daily_pet_respect_points"];
			bool hasFriendRequestsDisabled = MercuryEnvironment.EnumToBool(dRow["block_newfriends"].ToString());
			bool appearOffline = MercuryEnvironment.EnumToBool(dRow["hide_online"].ToString());
			bool hideInRoom = MercuryEnvironment.EnumToBool(dRow["hide_inroom"].ToString());
			uint currentQuestID = Convert.ToUInt32(mRow["quest_id"]);
			int currentQuestProgress = (int)mRow["quest_progress"];
			int achievementPoints = (int)mRow["achievement_score"];
			bool vIP = MercuryEnvironment.EnumToBool(dRow["vip"].ToString());
			double createDate = Convert.ToDouble(dRow["account_created"]);
			bool online = MercuryEnvironment.EnumToBool(dRow["online"].ToString());
			string citizenship = dRow["talent_status"].ToString();
			int belCredits = int.Parse(dRow["seasonal_currency"].ToString());
			uint favId = uint.Parse(mRow["favourite_group"].ToString());
			int lastChange = (int)dRow["last_name_change"];
			int regTimestamp = int.Parse(dRow["account_created"].ToString());
			bool tradeLocked = MercuryEnvironment.EnumToBool(dRow["trade_lock"].ToString());
			int tradeLockExpire = int.Parse(dRow["trade_lock_expire"].ToString());
            bool NuxPassed = MercuryEnvironment.EnumToBool(dRow["nux_passed"].ToString());

			return new Habbo(id, username, realName, ras, motto, look, gender, credits, activityPoints, lastActivityPointsUpdate, muted, homeRoom, respect, dailyRespectPoints, dailyPetRespectPoints, hasFriendRequestsDisabled, currentQuestID, currentQuestProgress, achievementPoints, regTimestamp, lastOnline, appearOffline, hideInRoom, vIP, createDate, online, citizenship, belCredits, group, favId, lastChange, tradeLocked, tradeLockExpire, NuxPassed);
		
            
        }
	}
}
