using System;
namespace Mercury.HabboHotel.Groups
{
	internal class GroupBackGroundColours
	{
		internal int Id;
		internal string Colour;
		internal GroupBackGroundColours(int Id, string Colour)
		{
			this.Id = Id;
			this.Colour = Colour;
		}
	}
}
