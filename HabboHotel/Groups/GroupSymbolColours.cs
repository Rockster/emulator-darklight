using System;
namespace Mercury.HabboHotel.Groups
{
	internal class GroupSymbolColours
	{
		internal int Id;
		internal string Colour;
		internal GroupSymbolColours(int Id, string Colour)
		{
			this.Id = Id;
			this.Colour = Colour;
		}
	}
}
