using System;
namespace Mercury.HabboHotel.Groups
{
	internal class GroupBaseColours
	{
		internal int Id;
		internal string Colour;
		internal GroupBaseColours(int Id, string Colour)
		{
			this.Id = Id;
			this.Colour = Colour;
		}
	}
}
