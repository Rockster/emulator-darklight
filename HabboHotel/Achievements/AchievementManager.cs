using Database_Manager.Database.Session_Details.Interfaces;
using Mercury.HabboHotel.Achievements.Composer;
using Mercury.HabboHotel.GameClients;
using Mercury.Messages;
using Mercury.Messages.Headers;
using System;
using System.Collections.Generic;
using System.Linq;
namespace Mercury.HabboHotel.Achievements
{
	public class AchievementManager
	{
		internal Dictionary<string, Achievement> Achievements;
        internal ServerMessage AchievementDataCached;

		internal AchievementManager(IQueryAdapter dbClient, out uint LoadedAchs)
		{
			this.Achievements = new Dictionary<string, Achievement>();
			this.LoadAchievements(dbClient);
            LoadedAchs = (uint)Achievements.Count;
		}
		internal void LoadAchievements(IQueryAdapter dbClient)
		{
            Achievements.Clear();
			AchievementLevelFactory.GetAchievementLevels(out this.Achievements, dbClient);
            AchievementDataCached = new ServerMessage(Outgoing.SendAchievementsRequirementsMessageComposer);
            AchievementDataCached.AppendInt32(this.Achievements.Count);
            foreach (Achievement Ach in this.Achievements.Values)
            {
                AchievementDataCached.AppendString(Ach.GroupName.Replace("ACH_", ""));
                AchievementDataCached.AppendInt32(Ach.Levels.Count);
                for (int i = 1; i < Ach.Levels.Count + 1; i++)
                {
                    AchievementDataCached.AppendInt32(i);
                    AchievementDataCached.AppendInt32(Ach.Levels[i].Requirement);
                }
            }
            AchievementDataCached.AppendInt32(0);
		}
		internal void GetList(GameClient Session, ClientMessage Message)
		{
			Session.SendMessage(AchievementListComposer.Compose(Session, this.Achievements.Values.ToList<Achievement>()));
		}
        internal void TryProgressLoginAchievements(GameClient Session)
        {
            if (Session.GetHabbo() == null)
            {
                return;
            }
            UserAchievement loginACH = Session.GetHabbo().GetAchievementData("ACH_Login");
            if (loginACH == null)
            {
                ProgressUserAchievement(Session, "ACH_Login", 1, true);
                return;
            }

            int daysBtwLastLogin = MercuryEnvironment.GetUnixTimestamp() - Session.GetHabbo().PreviousOnline;
            if (daysBtwLastLogin >= 51840 && daysBtwLastLogin <= 112320)
            {
                ProgressUserAchievement(Session, "ACH_Login", 1, true);
            }
        }
        internal void TryProgressRegistrationAchievements(GameClient Session)
        {
            if (Session.GetHabbo() == null)
            {
                return;
            }
            UserAchievement regACH = Session.GetHabbo().GetAchievementData("ACH_RegistrationDuration");
            if (regACH == null)
            {
                ProgressUserAchievement(Session, "ACH_RegistrationDuration", 1, true);
                return;
            }
            else if (regACH.Level == 5)
            {
                return;//Already Completed!
            }
            int sinceMember = MercuryEnvironment.GetUnixTimestamp() - (int)Session.GetHabbo().CreateDate;
            int daysSinceMember = Convert.ToInt32(Math.Round((double)259200 / 86400));

            if (daysSinceMember == regACH.Progress)
            {
                return;
            }
            int dais = daysSinceMember - regACH.Progress;
            if (dais < 1)
                return;

            ProgressUserAchievement(Session, "ACH_RegistrationDuration", dais, false);

        }
        internal void TryProgressHabboClubAchievements(GameClient Session)
        {
            if (Session.GetHabbo() == null || !Session.GetHabbo().GetSubscriptionManager().HasSubscription)
            {
                return;
            }

            UserAchievement ClubACH = Session.GetHabbo().GetAchievementData("ACH_VipHC");
            if (ClubACH == null)
            {
                ProgressUserAchievement(Session, "ACH_VipHC", 1, true);
                ProgressUserAchievement(Session, "ACH_BasicClub", 1, true);
                return;
            }
            else if (ClubACH.Level == 5)
            {
                return;//Already Completed!
            }
            var Subscription = Session.GetHabbo().GetSubscriptionManager().GetSubscription();
            int SinceActivation = MercuryEnvironment.GetUnixTimestamp() - Subscription.ActivateTime;

            if (SinceActivation < 31556926)
            {
                return;
            }
            if (SinceActivation >= 31556926)
            {
                ProgressUserAchievement(Session, "ACH_VipHC", 1, false);
                ProgressUserAchievement(Session, "ACH_BasicClub", 1, false);
            }
            if (SinceActivation >= 63113851)
            {
                ProgressUserAchievement(Session, "ACH_VipHC", 1, false);
                ProgressUserAchievement(Session, "ACH_BasicClub", 1, false);
            }
            if (SinceActivation >= 94670777)
            {
                ProgressUserAchievement(Session, "ACH_VipHC", 1, false);
                ProgressUserAchievement(Session, "ACH_BasicClub", 1, false);
            }
            if (SinceActivation >= 126227704)
            {
                ProgressUserAchievement(Session, "ACH_VipHC", 1, false);
                ProgressUserAchievement(Session, "ACH_BasicClub", 1, false);
            }
            if (SinceActivation >= 157784630)
            {
                ProgressUserAchievement(Session, "ACH_VipHC", 1, false);
                ProgressUserAchievement(Session, "ACH_BasicClub", 1, false);
            }


        }
		internal bool ProgressUserAchievement(GameClient Session, string AchievementGroup, int ProgressAmount, bool FromZero = false)
		{
			if (!this.Achievements.ContainsKey(AchievementGroup) || Session == null)
			{
				return false;
			}
			Achievement achievement = null;
			achievement = this.Achievements[AchievementGroup];
			UserAchievement userAchievement = Session.GetHabbo().GetAchievementData(AchievementGroup);
			if (userAchievement == null)
			{
				userAchievement = new UserAchievement(AchievementGroup, 0, 0);
				Session.GetHabbo().Achievements.Add(AchievementGroup, userAchievement);
			}
			int count = achievement.Levels.Count;
			if (userAchievement != null && userAchievement.Level == count)
			{
				return false;
			}
			checked
			{
				int num = (userAchievement != null) ? (userAchievement.Level + 1) : 1;
				if (num > count)
				{
					num = count;
				}
				AchievementLevel targetLevelData = achievement.Levels[num];
				int num2 = 0;
				if (FromZero)
				{
					num2 = ProgressAmount;
				}
				else
				{
					num2 = ((userAchievement != null) ? (userAchievement.Progress + ProgressAmount) : ProgressAmount);
				}
				int num3 = (userAchievement != null) ? userAchievement.Level : 0;
				int num4 = num3 + 1;
				if (num4 > count)
				{
					num4 = count;
				}
				if (num2 >= targetLevelData.Requirement)
				{
					num3++;
					num4++;
					int arg_E6_0 = num2 - targetLevelData.Requirement;
					num2 = 0;
					if (num == 1)
					{
						Session.GetHabbo().GetBadgeComponent().GiveBadge(AchievementGroup + num, true, Session, false);
					}
					else
					{
						Session.GetHabbo().GetBadgeComponent().RemoveBadge(Convert.ToString(AchievementGroup + (num - 1)), Session);
						Session.GetHabbo().GetBadgeComponent().GiveBadge(AchievementGroup + num, true, Session, false);
					}
					if (num4 > count)
					{
						num4 = count;
					}
					Session.GetHabbo().ActivityPoints += targetLevelData.RewardPixels;
					Session.GetHabbo().NotifyNewPixels(targetLevelData.RewardPixels);
					Session.GetHabbo().UpdateActivityPointsBalance();
					Session.SendMessage(AchievementUnlockedComposer.Compose(achievement, num, targetLevelData.RewardPoints, targetLevelData.RewardPixels));
					using (IQueryAdapter queryreactor = MercuryEnvironment.GetDatabaseManager().getQueryreactor())
					{
						queryreactor.setQuery(string.Concat(new object[]
						{
							"REPLACE INTO user_achievement VALUES (",
							Session.GetHabbo().Id,
							", @group, ",
							num3,
							", ",
							num2,
							")"
						}));
						queryreactor.addParameter("group", AchievementGroup);
						queryreactor.runQuery();
					}
					userAchievement.Level = num3;
					userAchievement.Progress = num2;
					Session.GetHabbo().AchievementPoints += targetLevelData.RewardPoints;
					Session.GetHabbo().NotifyNewPixels(targetLevelData.RewardPixels);
					Session.GetHabbo().ActivityPoints += targetLevelData.RewardPixels;
					Session.GetHabbo().UpdateActivityPointsBalance();
					Session.SendMessage(AchievementScoreUpdateComposer.Compose(Session.GetHabbo().AchievementPoints));
					AchievementLevel targetLevelData2 = achievement.Levels[num4];
					Session.SendMessage(AchievementProgressComposer.Compose(achievement, num4, targetLevelData2, count, Session.GetHabbo().GetAchievementData(AchievementGroup)));
					Talent talent = null;
					if (MercuryEnvironment.GetGame().GetTalentManager().TryGetTalent(AchievementGroup, out talent))
					{
						MercuryEnvironment.GetGame().GetTalentManager().CompleteUserTalent(Session, talent);
					}
					return true;
				}
				userAchievement.Level = num3;
				userAchievement.Progress = num2;
				using (IQueryAdapter queryreactor2 = MercuryEnvironment.GetDatabaseManager().getQueryreactor())
				{
					queryreactor2.setQuery(string.Concat(new object[]
					{
						"REPLACE INTO user_achievement VALUES (",
						Session.GetHabbo().Id,
						", @group, ",
						num3,
						", ",
						num2,
						")"
					}));
					queryreactor2.addParameter("group", AchievementGroup);
					queryreactor2.runQuery();
				}
				Session.SendMessage(AchievementProgressComposer.Compose(achievement, num, targetLevelData, count, Session.GetHabbo().GetAchievementData(AchievementGroup)));

                Session.GetMessageHandler().GetResponse().Init(Outgoing.UpdateUserDataMessageComposer);
                Session.GetMessageHandler().GetResponse().AppendInt32(-1);
                Session.GetMessageHandler().GetResponse().AppendString(Session.GetHabbo().Look);
                Session.GetMessageHandler().GetResponse().AppendString(Session.GetHabbo().Gender.ToLower());
                Session.GetMessageHandler().GetResponse().AppendString(Session.GetHabbo().Motto);
                Session.GetMessageHandler().GetResponse().AppendInt32(Session.GetHabbo().AchievementPoints);
                Session.GetMessageHandler().SendResponse();
                return false;
			}
		}
		internal Achievement GetAchievement(string AchievementGroup)
		{
			if (this.Achievements.ContainsKey(AchievementGroup))
			{
				return this.Achievements[AchievementGroup];
			}
			return null;
		}
	}
}
