using Mercury.Messages;
using Mercury.Messages.Headers;
using System;
namespace Mercury.HabboHotel.Achievements.Composer
{
	internal class AchievementScoreUpdateComposer
	{
		internal static ServerMessage Compose(int Score)
		{
			ServerMessage serverMessage = new ServerMessage(Outgoing.AchievementPointsMessageComposer);
			serverMessage.AppendInt32(Score);
			return serverMessage;
		}
	}
}
