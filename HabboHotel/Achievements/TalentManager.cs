using Database_Manager.Database.Session_Details.Interfaces;
using Mercury.HabboHotel.GameClients;
using Mercury.HabboHotel.Items;
using Mercury.Messages;
using Mercury.Messages.Headers;
using System;
using System.Collections.Generic;
using System.Data;
namespace Mercury.HabboHotel.Achievements
{
	internal class TalentManager
	{
		private Dictionary<int, Talent> Talents;
		internal TalentManager()
		{
			this.Talents = new Dictionary<int, Talent>();
		}
		internal void Initialize(IQueryAdapter dbClient)
		{
			dbClient.setQuery("SELECT * FROM achievements_talenttrack ORDER BY `order_num` ASC");
			DataTable table = dbClient.getTable();
			foreach (DataRow dataRow in table.Rows)
			{
				Talent talent = new Talent((int)dataRow["id"], (string)dataRow["type"], (int)dataRow["parent_category"], (int)dataRow["level"], (string)dataRow["achievement_group"], (int)dataRow["achievement_level"], (string)dataRow["prize"], (uint)dataRow["prize_baseitem"]);
				this.Talents.Add(talent.Id, talent);
			}
		}
		internal Talent GetTalent(int TalentId)
		{
			return this.Talents[TalentId];
		}
		internal bool LevelIsCompleted(GameClient Session, string TrackType, int TalentLevel)
		{
			foreach (Talent current in this.GetTalents(TrackType, TalentLevel))
			{
				if (Session.GetHabbo().GetAchievementData(current.AchievementGroup) != null && Session.GetHabbo().GetAchievementData(current.AchievementGroup).Level >= current.AchievementLevel)
				{
					return false;
				}
			}
			return true;
		}
		internal void CompleteUserTalent(GameClient Session, Talent Talent)
		{
            if (Session == null || Session.GetHabbo() == null || Session.GetHabbo().CurrentTalentLevel < Talent.Level || Session.GetHabbo().Talents.ContainsKey(Talent.Id))
            {
                return;
            }
			if (!this.LevelIsCompleted(Session, Talent.Type, Talent.Level))
			{
				return;
			}
			if (Talent.Prize != "" && Talent.PrizeBaseItem > 0u)
			{
				Item item = MercuryEnvironment.GetGame().GetItemManager().GetItem(Talent.PrizeBaseItem);
				MercuryEnvironment.GetGame().GetCatalog().DeliverItems(Session, item, 1, "", 0, 0, "");
			}
			UserTalent value = new UserTalent(Talent.Id, 1);
			Session.GetHabbo().Talents.Add(Talent.Id, value);
			using (IQueryAdapter queryreactor = MercuryEnvironment.GetDatabaseManager().getQueryreactor())
			{
				queryreactor.runFastQuery(string.Concat(new object[]
				{
					"REPLACE INTO user_talents VALUES (",
					Session.GetHabbo().Id,
					", ",
					Talent.Id,
					", ",
					1,
					");"
				}));
			}
			ServerMessage serverMessage = new ServerMessage(Outgoing.TalentLevelUpMessageComposer);
			serverMessage.AppendString(Talent.Type);
			serverMessage.AppendInt32(Talent.Level);
			serverMessage.AppendInt32(0);
            if (Talent.Type == "citizenship" && Talent.Level == 4)
            {
                serverMessage.AppendInt32(2);
                serverMessage.AppendString("HABBO_CLUB_VIP_7_DAYS");
                serverMessage.AppendInt32(7);
                serverMessage.AppendString(Talent.Prize);
                serverMessage.AppendInt32(0);
            }
            else
            {
                serverMessage.AppendInt32(1);
                serverMessage.AppendString(Talent.Prize);
                serverMessage.AppendInt32(0);
            }
			
			Session.SendMessage(serverMessage);

            if (Talent.Type == "citizenship")
            {
                if (Talent.Level == 3)
                {
                    MercuryEnvironment.GetGame().GetAchievementManager().ProgressUserAchievement(Session, "ACH_Citizenship", 1);
                }
                else if (Talent.Level == 4)
                {
                    Session.GetHabbo().GetSubscriptionManager().AddSubscription(7);
                    using (IQueryAdapter queryreactor = MercuryEnvironment.GetDatabaseManager().getQueryreactor())
                    {
                        queryreactor.runFastQuery(string.Concat(new object[]
				{
					"UPDATE users SET talent_status = 'helper' WHERE id = ",
					Session.GetHabbo().Id,
					";"
				}));
                    }
                }
            }
		}
		internal bool TryGetTalent(string AchGroup, out Talent talent)
		{
			foreach (Talent current in this.Talents.Values)
			{
				if (current.AchievementGroup == AchGroup)
				{
					talent = current;
					return true;
				}
			}
			talent = null;
			return false;
		}
		internal Dictionary<int, Talent> GetAllTalents()
		{
			return this.Talents;
		}
		internal List<Talent> GetTalents(string TrackType, int ParentCategory)
		{
			List<Talent> list = new List<Talent>();
			foreach (Talent current in this.Talents.Values)
			{
				if (current.Type == TrackType && current.ParentCategory == ParentCategory)
				{
					list.Add(current);
				}
			}
			return list;
		}
	}
}
