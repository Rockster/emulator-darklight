﻿/*
 * Created by SharpDevelop.
 * User: claudio.santoro
 * Date: 02/10/2014
 * Time: 16:55
 * 
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
 */
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Mercury
{

    abstract class AbstractBar
    {
        public AbstractBar()
        {

        }

        /// <summary>
        /// Prints a simple message 
        /// </summary>
        /// <param name="msg">Message to print</param>
        public void PrintMessage(string msg)
        {
            Console.Write("  " + msg);
            Console.Write("\r".PadLeft(Console.WindowWidth - Console.CursorLeft - 1));
        }

        public abstract void Step();
    }


    class AnimatedBar : AbstractBar
    {
        List<string> animation;
        int counter;
        public AnimatedBar()
            : base()
        {
            this.animation = new List<string> { "/", "-", @"\", "|" };
            this.counter = 0;
        }

        /// <summary>
        /// prints the character found in the animation according to the current index
        /// </summary>
        public override void Step()
        {
            Console.Write(this.animation[this.counter] + "\b");
            this.counter++;
            if (this.counter == this.animation.Count)
                this.counter = 0;
        }
    }
    class SwayBar : AbstractBar
    {
        string bar;
        string pointer;
        string blankPointer;
        int counter;
        direction currdir;
        enum direction { right, left };
        public SwayBar()
            : base()
        {
            this.bar = "|                         |";
            this.pointer = "===";
            this.blankPointer = this.BlankPointer();
            this.currdir = direction.right;
            this.counter = 1;
        }

        /// <summary>
        /// sets the atribute blankPointer with a empty string the same length that the pointer
        /// </summary>
        /// <returns>A string filled with space characters</returns>
        private string BlankPointer()
        {
            StringBuilder blank = new StringBuilder();
            for (int cont = 0; cont < this.pointer.Length; cont++)
                blank.Append(" ");
            return blank.ToString();
        }

        /// <summary>
        /// reset the bar to its original state
        /// </summary>
        private void ClearBar()
        {
            this.bar = this.bar.Replace(this.pointer, this.blankPointer);
        }

        /// <summary>
        /// remove the previous pointer and place it in a new possition
        /// </summary>
        /// <param name="start">start index</param>
        /// <param name="end">end index</param>
        private void PlacePointer(int start, int end)
        {
            this.ClearBar();
            this.bar = this.bar.Remove(start, end);
            this.bar = this.bar.Insert(start, this.pointer);
        }

        /// <summary>
        /// prints the progress bar acorrding to pointers and current direction
        /// </summary>
        public override void Step()
        {
            if (this.currdir == direction.right)
            {
                this.PlacePointer(counter, this.pointer.Length);
                this.counter++;
                if (this.counter + this.pointer.Length == this.bar.Length)
                    this.currdir = direction.left;
            }
            else
            {
                this.PlacePointer(counter - this.pointer.Length, this.pointer.Length);
                this.counter--;
                if (this.counter == this.pointer.Length)
                    this.currdir = direction.right;
            }
            Console.Write(this.bar + "\r");
        }
    }
}