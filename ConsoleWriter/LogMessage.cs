using System;
namespace ConsoleWriter
{
    internal class LogMessage
    {
        internal string Message;
        internal string location;

        public LogMessage(string Message, string location)
        {
            this.Message = Message;
            this.location = location;
        }
        internal void Dispose()
        {
            this.Message = null;
            this.location = null;
        }
    }
}