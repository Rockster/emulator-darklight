using System.Globalization;
namespace Mercury
{
	internal static class TextHandling
	{
        private static NumberFormatInfo nfi = new NumberFormatInfo();

        internal static void replaceDecimal()
        {
            nfi.NumberDecimalSeparator = ".";
        }

		internal static string GetString(double k)
		{
            return k.ToString(nfi);
		}
	}
}
